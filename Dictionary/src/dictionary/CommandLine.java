/** This program simulates a Vi-E dictionary
 * @author Nguyen Ngoc Thanh Tung 17021353 + Hoang Son Tung 17021351
 * @since 2018 20 09
 * @version 1.0
 */

// Declare libraries
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.IOException;

/** This class describes one word in dictionary
 */
class Word {
    // Declare properties
    private String word_target;
    private String word_explain;

    /**
     * Getter/Setter
     */
    public void setWord_target(String str){
        this.word_target = str;
    }

    public void setWord_explain(String str){
        this.word_explain = str;
    }

    public String getWord_explain() {
        return word_explain;
    }

    public String getWord_target() {
        return word_target;
    }
}

/** This class manages all of words in your dictionary
 */
class Dictionary {
    // Declare properties
    public ArrayList<Word> words = new ArrayList<>();
    public int size;
}

/** This class includes operations in a dictionary
 */
class DictionaryManagement {

    public Dictionary dict = new Dictionary();

    /** This method is used to add new words into their dictionary
     *  Users enter the number of new words they want to add into dictionary, then enter the words and its meaning
     */
    public void insertFromCommandline() {
        System.out.print("How many new words do you want to add into dictionary: ");
        Scanner scanInput = new Scanner(System.in);

        dict.size = scanInput.nextInt();


        for (int i=0;i<dict.size;i++) {
            System.out.println("--------------------------");
            Word newWord = new Word();

            System.out.print("Enter English word: ");
            String wordE = scanInput.next();
            newWord.setWord_target(wordE);

            scanInput.nextLine();

            System.out.print("Enter the meaning: ");
            String wordV = scanInput.nextLine();
            newWord.setWord_explain(wordV);

            dict.words.add(newWord);
        }

        System.out.println("------------------------------------------");
        System.out.println("You have added "+ dict.size + " new words into dictionary!");
        System.out.println();
        System.out.println();
    }

    public void insertFromFile() throws IOException{
        String filePath = "/Users/megatunger/Desktop/evidictionary/dictionaries.txt";
        FileInputStream file = new FileInputStream(filePath);
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            String wordE = "", wordV= "";
            int index=0;
            for (int i=0;i<line.length();i++){
                if ((int)line.charAt(i) !=  9){
                    wordE += line.charAt(i);
                }else{
                    index=i;
                    break;
                }
            }
            for (int i=index+1;i<line.length();i++){
                wordV+=line.charAt(i);
            }

            Word newWord = new Word();
            wordE = wordE.trim();
            wordV = wordV.trim();

            newWord.setWord_explain(wordV);
            newWord.setWord_target(wordE);

            dict.words.add(newWord);
            dict.size++;
        }
        scanner.close();
    }

    /** This method look up for a specific word in your dictionary
     * @return meaning of that word
     */
    public void dictionaryLookup(){
        System.out.print("What English word do you want to look up?: ");
        Scanner scanner = new Scanner(System.in);
        String  word = scanner.nextLine();
        boolean doesExist = false;
        for (int i=0;i<dict.size;i++){
            if (word.compareTo(dict.words.get(i).getWord_target())==0){
                word = dict.words.get(i).getWord_explain();
                doesExist = true;
                break;
            }
        }
        if (doesExist == true) System.out.print(word);
        else System.out.print("Khong co");
    }

    /** This method edit a word's meaning to your dictionary
     * @return nothing, a dictionary with edited word
     */
    public void editWordOfDictionary() {
        System.out.println();
        System.out.print("Which word do you want to change: ");
        Scanner scanner = new Scanner(System.in);
        String  word = scanner.nextLine();
        boolean doesExist = false;
        for (int i=0;i<dict.size;i++){
            System.out.print(dict.words.get(i).getWord_target());
            if (word.compareTo(dict.words.get(i).getWord_target())==0){
                System.out.println();
                System.out.println("Nghia ban dau la: " + dict.words.get(i).getWord_explain());
                System.out.println("Hay nhap nghia muon sua lai: ");
                word = scanner.nextLine();
                dict.words.get(i).setWord_explain(word);
                doesExist = true;
                break;
            }
        }
        if (doesExist == true) System.out.print("Da sua");
        else System.out.print("Khong tim thay tu can sua!");
    }

    /** This method delete a word in your dictionary
     * @return nothing, a dictionary with removed word
     */
    public void deleteWordOfDictionary() {
        System.out.println();
        System.out.print("Which word do you want to delete: ");
        Scanner scanner = new Scanner(System.in);
        String  word = scanner.nextLine();
        boolean doesExist = false;
        for (int i=0;i<dict.size;i++){
            if (word.compareTo(dict.words.get(i).getWord_target())==0) {
                dict.words.remove(i);
                doesExist = true;
                dict.size--;
                break;
            }
        }
        if (doesExist == true) System.out.println("Da xoa");
        else System.out.print("Khong tim thay tu can xoa!");
    }

    /** This method export your dictionary
     * @return nothing, export to disk a dictionary text file
     */

    public void dictionaryExportToFile() throws IOException {
        String filePathExport = "export.txt";
        FileOutputStream file = new FileOutputStream(filePathExport);
        for (int i=0; i<dict.words.size(); i++) {
            String str="";
            str = dict.words.get(i).getWord_target() + " " + dict.words.get(i).getWord_explain() + "\n";
            byte[] strToBytes = str.getBytes();
            file.write(strToBytes);
        }
    }
}

/** This class
 */
class DictionaryCommandline{
    public DictionaryManagement management = new DictionaryManagement();

    /** This method shows all words in your dictionay
     * @return The list of all words
     */
    public void showAllWords(){
        System.out.println("This is your dictionary: ");
        System.out.println("No      | English          | Vietnamese");
        for (int i=0;i<management.dict.size;i++){
            System.out.print(i+"       | "+management.dict.words.get(i).getWord_target());
            for (int j=1;j<=(16-management.dict.words.get(i).getWord_target().length()+1);j++){
                System.out.print(" ");
            }
            System.out.println("| " + management.dict.words.get(i).getWord_explain());
        }
        System.out.println();
        System.out.println();
    }

    /** This method helps user add new words and print the all of words in dictionary
     */
    public void DictionaryBasic(){
        management.insertFromCommandline();
        this.showAllWords();
    }

    public void DictionaryAdvance() throws IOException{
        management.insertFromFile();
        this.showAllWords();
        management.dictionaryLookup();
        //management.editWordOfDictionary();
        //management.deleteWordOfDictionary();
        management.dictionaryExportToFile();
        //this.showAllWords();
    }
}

/** This class is a Vi-E dictionary program
 *
 */
public class CommandLine {
    public static void main(String[] args) throws  IOException{
        DictionaryManagement test = new DictionaryManagement();
        //test.insertFromCommandline();
        //test.insertFromFile();
        //test.dictionaryLookup();

       // test.editWordOfDictionary();

        DictionaryCommandline t = new DictionaryCommandline();
        t.DictionaryAdvance();


    }
}
